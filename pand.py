import os
import json
from pandas import read_csv

cur_dir = os.path.dirname(os.path.abspath(__file__))  # current dir
name_csv = 'Armani.csv'  # name of csv file
df1 = read_csv(os.path.join(cur_dir, name_csv))  # dataframe of file csv

json_data = {}

region = df1.groupby('region').size()  # count of items in regions
color = df1.groupby('color').size() / df1.groupby('color').size().sum()*100  # percent colors
size = df1.groupby('size').size() / df1.groupby('size').size().sum()*100  # percent size
desc = df1.groupby('description').size() / df1.groupby('description').size().sum()*100  # percent of desc


'''Collecting data to dictionary
'''
json_data['region'] = region.to_json()
json_data['color'] = color.to_json()
json_data['size'] = size.to_json()
json_data['description'] = desc.to_dict()


'''Save test to json file
'''
with open('../test.json', 'w') as f:
    json.dump(json_data, f)
