from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import HtmlXPathSelector
from orphanage.items import ArmaniItem
from datetime import datetime


"""Spider to collect items from site armani.com

"""
class ArmaniProducts(CrawlSpider):
    name = 'armani_items'
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0'
    allowed_domains = ['www.armani.com']

    def __init__(self, region=None, *args, **kwargs):
        super(ArmaniProducts, self).__init__(*args, **kwargs)
        if region and region != 'all' and (region.lower() == 'us' or region.lower() == 'fr'):
            self.start_urls = ['http://www.armani.com/{0}/giorgioarmani'.format(region.lower())]

    start_urls = [
        'http://www.armani.com/us/giorgioarmani',
        'http://www.armani.com/fr/giorgioarmani',
    ]

    rules = (
        Rule(LinkExtractor(allow=('.*_cod.*\.html',)),
                           callback='parse_item', follow=True),

        Rule(LinkExtractor(allow=(
                                  'http://www.armani.com/us/giorgioarmani/',
                                  'http://www.armani.com/fr/giorgioarmani/')),
                           follow=True),
    )


    def parse_item(self, response):
        self.logger.info('Page item: {0}, time: {1}'.format(response.url, datetime.now().strftime("%Y.%m.%d %H:%M")))

        hxs = HtmlXPathSelector(response)
        item = ArmaniItem()

        item['time'] = datetime.now().strftime("%Y.%m.%d %H:%M")
        item['name'] = hxs.select('//h2[@class="productName"]/text()').extract()

        if hxs.select('//span[@class="priceValue"]/text()'):
            item['price'] = hxs.select('//span[@class="priceValue"]/text()')[0].extract()
        else:
            item['price'] = 'None'

        if hxs.select('//span[@class="currency"]/text()'):
            if hxs.select('//span[@class="currency"]/text()')[0].extract() == '$':
                item['currency'] = 'USD'
            else:
                item['currency'] = hxs.select('//span[@class="currency"]/text()')[0].extract()
        else:
            item['currency'] = 'None'
        if hxs.select('//li[@class="selected leaf"]/a/text()'):
            item['category'] = hxs.select('//li[@class="selected leaf"]/a/text()').extract()
        else:
            item['category'] = 'None'
        if hxs.select('//span[@class="MFC"]/text()'):
            item['sku'] = hxs.select('//span[@class="MFC"]/text()').extract()
        else:
            item['sku'] = 'None'
        if hxs.select('//span[@class="outStock"]/text()').extract() \
                or hxs.select('//div[@class="soldOutButton"]/text()').extract():
            item['available'] = 'No'
        else:
            item['available'] = 'Yes'
        if hxs.select('//*[contains(@id, "color_")]/a/text()'):
            item['color'] = hxs.select('//*[contains(@id, "color_")]/a/text()').extract()
        else:
            item['color'] = 'None'

        if hxs.select('//*[contains(@id, "sizew_")]/a/text()'):
            item['size'] = hxs.select('//*[contains(@id, "sizew_")]/a/text()').extract()
        else:
            item['size'] = None

        if 'armani.com/us/' in response.url:
            item['region'] = 'US'
        elif 'armani.com/fr/' in response.url:
            item['region'] = 'FR'

        if hxs.select('//div[@class="descriptionContent"]/text()'):
            item['description'] = hxs.select('//div[@class="descriptionContent"]/text()')[0].extract()
        else:
            item['description'] = 'None'

        self.logger.info('Item scrapped: {0}'.format(item))

        yield item
