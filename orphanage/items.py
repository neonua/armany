# -*- coding: utf-8 -*-


from scrapy import  Item, Field


class ArmaniItem(Item):
    name = Field()
    price = Field()
    currency = Field()
    category = Field()
    sku = Field()
    available = Field()
    time = Field()
    color = Field()
    size = Field()
    region = Field()
    description = Field()



